#![no_std]
#![forbid(unsafe_code)]
#![doc = include_str!("../README.md")]
pub mod commutative;
pub use commutative::*;
