#[macro_export]
macro_rules! match_commutative {
    (
        $operant1_expr:expr,
        $bin_op_expr:expr,
        $operant2_expr:expr,
        $( $operant1:pat, $bin_op:pat, $operant2:pat $(if $if_expr:expr)? => $block_to_execute:block ), +
        $(non_commut { $( $operant1_non_commut:pat, $bin_op_non_commut:pat, $operant2_non_commut:pat $(if $if_expr_non_commut:expr)? => $block_to_execute_non_commut:block ), + })?
    ) => {
        {
            match ($operant1_expr, $bin_op_expr, $operant2_expr) {
                $(
                    ($operant1, $bin_op, $operant2) | ($operant2, $bin_op, $operant1) $(if $if_expr)? => $block_to_execute
                )+
                $($(
                    ($operant1_non_commut, $bin_op_non_commut, $operant2_non_commut) $(if $if_expr_non_commut)? => $block_to_execute_non_commut
                )+)?
            }
        }
    };
}
