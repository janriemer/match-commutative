# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- None

## 0.1.0 (16. September, 2023)
### Added
- Add the ability to use optional `if` guards at the end of match arm (like std Rust) ([!1](https://gitlab.com/janriemer/match-commutative/-/merge_requests/1))
```rust
use match_commutative::match_commutative;

let operant1 = Operant::Str(Some("42".into()));
let operant2 = Operant::Num(Some(1));
let operator = Operator::Plus;

match_commutative!(
    operant1,
    operator,
    operant2,
    Operant::Str(Some(operant_str)),
    Operator::Plus,
    // we can add an optional if guard at the end of the match arm, just like in std Rust
    Operant::Num(Some(operant_num)) if operant_str.len() < 3 => {
        let result = operant_num + operant_str.parse::<isize>().unwrap();
        println!("Result is: {}", result);
    },
    Operant::Str(Some(operant_str)),
    Operator::Mult,
    Operant::Num(Some(operant_num)) if operant_str.len() < 3 => {
        let result = operant_num * operant_str.parse::<isize>().unwrap();
        println!("Result is: {}", result);
    }
    non_commut {
        _, _, _ => {
            panic!("Not relevant for this example")
        }
    }
);
```

## 0.1.0-alpha.1 (04. June, 2023)
### Fixed
- Cargo.toml: fix non-existant/misspelled category [5d24f5e](https://gitlab.com/janriemer/match-commutative/-/commit/5d24f5ec2e01d31e6e5a1e19429d5381fb54a5fd)

## 0.1.0-alpha.0 (04. June, 2023)

- Initial version